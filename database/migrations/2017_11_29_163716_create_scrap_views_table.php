<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScrapViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scrap_views', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id')->unique();
            $table->string('facebook_id');
            $table->integer('metrics_id')->unique();
            $table->integer('views')->default(0);
            $table->integer('sent')->default(0);
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scrap_views');
    }
}
