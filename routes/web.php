<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Establece rutas, asigna controladores a las rutas, se asignan vistas pero esto realmente se hace en los controladores
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');



//Facebook
Route::get('/get-metrics', 'ScrapController@sendResult');
Route::get('/get-material', 'ScrapController@startScrap');
Route::get('/get-frozen', 'ScrapController@sendFrozen');
Route::get('/dump-html/{facebook_id}/', 'ScrapController@checkHtml');
Route::get('/dump-html/{facebook_id}/{estado}', 'ScrapController@updateHTML');

Route::post('/views-scrapper', 'ScrapController@scrapPage');

Route::post('/manual-post', 'ScrapInstaController@scrapManualPost');
Route::post('/get-posts-bundle', 'ScrapInstaController@scrapProfilePosts');
Route::get('/get-insta-dates', 'ScrapInstaController@retrieveDates');
Route::post('/save-posts-info', 'ScrapInstaController@savePostInfo');
Route::post('/save-data-posts', 'ScrapInstaController@savePostsDates');
Route::get('/instascrap/{url}', 'ScrapInstaController@dumpInstaChannel');
Route::post('/save-insta-profile', 'ScrapInstaController@scrapProfile');
Route::get('/get-insta-metrics', 'ScrapInstaController@sendMetrics');
Route::get('/get-insta-posts', 'ScrapInstaController@sendPosts');
Route::get('/scrap-profiles', 'ScrapInstaController@startProfileScrap');
Route::get('/get-profile-followers', 'ScrapInstaController@sendInstaFollowers');
Route::get('/scrap-insta-metrics', 'ScrapInstaController@scrapPostsMetrics');
//Debug methods
// Route::get('/debug-metrics', 'ScrapInstaController@scrapPostsMetrics');
 Route::get('/debug-posts-bundle', 'ScrapInstaController@debugScrapInitialPosts');
 //Route::get('/debug-scrap', 'ScrapInstaController@debugScrap');

