<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Goutte\Client as GoutteClient;
use JonnyW\PhantomJs\Client as PhantomClient;
use Symfony\Component\DomCrawler\Crawler;
use App\ScrapView;
use GuzzleHttp\Psr7\Request as GuzzRequest;
use GuzzleHttp\Client as GuzzClient; 

class ScrapController extends Controller
{     
 /*-------------------------------------------------------*
 * Se obtiene los posts de  video de la base de datos 
 principal y se guardan para buscar su metrica de vistas
 *-------------------------------------------------------*/
 function scrapPage(Request $scraps){
    //Asi se guardan los datos
    //$data_request['facebook_id']; 
    //Se obtiene el primer elemento del scrap 
  $data_requests = json_decode($scraps[0], true);
    //Se recorre el scrap  y se obtiene cada objeto con los valores de los videos
  foreach ($data_requests as $data_request) {
     //Se crean los valores en la base de datos
    $scrap_view = ScrapView::firstOrNew(array('post_id' => $data_request['post_id']));
    $scrap_view->facebook_id = $data_request['facebook_id'];
    $scrap_view->metrics_id = $data_request['metrics_id'];
    $state = $data_request['scrap_state'];
    if($state>0){
        $scrap_view->sent = 10;
    }else{
        $scrap_view->sent = 0;
    }
    $scrap_view->save(); 
  }
  return "Datos guardados con éxito";
}
 /*-------------------------------------------------------*
 * Recorre los posts para sacar el dato de reproducciones
 *-------------------------------------------------------*/
 function startScrap(){

   $client = PhantomClient::getInstance();    
   // $client->getEngine()->setPath('/var/http/owak/live/scraper/bin/phantomjs');
   $client->getEngine()->setPath(base_path().'/bin/phantomjs');
   $views = ScrapView::where('views', "=" , 0)->whereIn('sent', [-6, -5, -4, -3, -2, -1, 0, 2, 3, 4, 5 ,6 ,7 , 8, 9, 10])->get();
 
   $delay = 15;
   $countSuccess = 0;
   $countFail = 0;
   $countTotal = 0;
   $countFrozen = 0;
   
   foreach ($views as $view) {
        # code...
     

     $request = $client->getMessageFactory()->createRequest('https://www.facebook.com/'.$view->facebook_id, 'GET');
     $response = $client->getMessageFactory()->createResponse();
    if($view->sent < -3 || $view->sent > 6){
      $delay=25;
    }
     $request->setDelay($delay);
     $client -> send($request, $response);
     if($response->getStatus() == 200){
      $html = $response->getContent();
      $css_selector = 'div._1vx9 span';
      $crawler = new Crawler();
      $crawler->addHTMLContent($html);
      $output = $crawler->filter($css_selector)->extract('_text');
      $vistas = 0;
      $found = false;
      foreach ($output as $n) {
        if(strpos($n, "Views") || strpos($n, "reproducciones")){
          $vistas = intval(str_replace(["," , "."],"",explode(" ", $n,2)[0]));
          $found = true;
        }
      }
      if($found == true){
       $view->views = $vistas;
       $countSuccess ++;
     }else{
       $view->sent = $view->sent - 1;
        $countFail ++;
     }
     if($view->sent <= -6){
       $countFrozen ++;
     }
     $countTotal++;
     $view->save();
   }
 }
 
 return array($countSuccess, $countFail, $countTotal, $countFrozen);
}
/*-------------------------------------------------------*
 * Observa los valores
 *-------------------------------------------------------*/
function checkHtml($facebook_id){
  $client = PhantomClient::getInstance();
  $client->getEngine()->setPath(base_path().'/bin/phantomjs');
  $delay = 15;
  $successSignal = 0;
  $failSignal = 0;
  $totalSignal = 0;
   
  $request = $client->getMessageFactory()->createRequest('https://www.facebook.com/'.$facebook_id, 'GET');
  $response = $client->getMessageFactory()->createResponse();
  $request->setDelay($delay);
  $client -> send($request, $response);
  if($response->getStatus() == 200){
    $html = $response->getContent();
    $css_selector = 'div._1vx9 span';
    $crawler = new Crawler();
    $crawler->addHTMLContent($html);
    $output = $crawler->filter($css_selector)->extract('_text');
    $vistas = 0;
    $found = false;
    foreach ($output as $n) {
      if(strpos($n, "Views") || strpos($n, "reproducciones")){
        $vistas = intval(str_replace(["," , "."],"",explode(" ", $n,2)[0]));
        $found = true;
      }
    }
    if($found == true){
     $successSignal ++;
   }else{
     $failSignal ++;
   }
   $totalSignal++;
   if(is_numeric($vistas) && $vistas > 0){
      $updateView = ScrapView::where('facebook_id', "LIKE", "%".$facebook_id)->first();
      $updateView->views = $vistas; 
      $updateView->save();
   }
   echo $html;
   echo '<a href="0" style="background-color:green; padding:10px; color:white; margin:10px; text-decoration:none;"> Renovar </a>';
   echo '<a href="-10" style="background-color:red; padding:10px; color:white; margion:10px; text-decoration:none;"> Congelar </a>';
   var_dump($output);
 }
}

/*-------------------------------------------------------*
 * Actualiza el estado del post para renovarlo o congelarlo
 *-------------------------------------------------------*/
function updateHtml($facebook_id, $estado){
   $updateView = ScrapView::where('facebook_id', "LIKE", "%".$facebook_id)->first();
   if($updateView->sent <= -6 && $estado == 0){
       $updateView->sent = $estado; 
   }else if($updateView->sent <= -6 && $estado == -10){
      $updateView->sent = $estado;
   }
    $updateView->save();
}
 /*-------------------------------------------------------*
 * Prepara los posts de video que tienen vistas para 
 enviarlos a la base de datos principal 
 *-------------------------------------------------------*/
 function sendResult(){
  $container_views = [];
//$index = 0;
//$container_views[$index] = [];
  $scrap_views = ScrapView::where('views', '>' , 0)->get();
  foreach($scrap_views as $scrap_view){

    $response_pack = [
    'post_id' => $scrap_view->post_id,
    'metrics_id' => $scrap_view->metrics_id,
    'views' => $scrap_view->views
    ];

    array_push($container_views, $response_pack);

    $scrap_view->sent = 1;
    $scrap_view->save();
  }
/*// $response_result = [];
// foreach ($container_views as $container_view) {
//         $response_encode = json_encode($container_view);
//          $response_result =  array_merge($response_result , $response_encode);
}
*/
return $container_views;
}


function sendFrozen(){
     $container_frozen=[];
     $frozen_scraps = ScrapView::where('sent', '<=', '-6')->where('views', 0)->get();

     foreach($frozen_scraps as $frozen_scrap){
       $response_pack =[
          'post_id' => $frozen_scrap->post_id,
          'scrap_state' => $frozen_scrap->sent
       ];

       array_push($container_frozen, $response_pack);
     }
     return $container_frozen; 
  }
 /*-------------------------------------------------------*
 * Verifica las metricas y en caso de que ya sean enviadas las borra.
 *-------------------------------------------------------*/

 function deleteData(){

   $sent_scrap = ScrapView::where('sent', '=', 1)->delete();

 }
}
