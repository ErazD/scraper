<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstagramPost extends Model
{
	protected $guarded = [];

	public function add($post, $profile_id)
	{	
		//Agregar post nuevo
			//Se crea un post
		$post_created = $this->create([
			'profile_id' => $profile_id,
			'post_id' => $post['post_id'],
			'content' => isset($post['content']) ? (string) $post['content'] : null,
			'published' => $post['published'],
			'type' => $post['type'],
			'permalink_url' => isset($post['url']) ? $post['url'] : '#',
			'attachment' => isset($post['attachment'])? $post['attachment'] : null,
			'likes' => isset($post['likes'])? $post['likes'] : 0,
			'views' => isset($post['views'])? $post['views'] : 0,
			'comments' => isset($post['comments'])? $post['comments'] : 0
			]);

			//Se crea la métrica inicial de ese post
	}
}

