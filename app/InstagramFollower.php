<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstagramFollower extends Model
{
    protected $guarded = [];

    public function add($follower ,$profile){
    	$follower = $this->create([
        'profile_id' => $profile,
        'table' => 'insta_profiles',
        'metric' => $follower
    	]);
    }
}
