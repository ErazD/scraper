<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\ScrapController;
use DateTime;


class StartScrap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Scrap:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start scraping the data recieved from Owak Graph';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(ScrapController $start)
    {
        //
        $this->info(date("Y-m-d H:i:s") . 'Inicio de scrap de datos');
        $this->startScrap($start);
       
    }
    protected function startScrap(ScrapController $scrap){
        $result = $scrap->startScrap();
        $this->info(date("Y-m-d H:i:s") . 'Scrap de datos terminado' . 'Total Posts: ' . $result[2] . '\n' . 'Post Scrapeados: ' . $result[0] . '\n' . 'Post fallidos: ' . $result[1] . '\n' . 'Posibles post congelados:' . $result[3] . 'Numero de pausas en el codigo: '  . $result[4]);
    }

}
