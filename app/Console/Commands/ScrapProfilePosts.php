<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\ScrapInstaController;

class ScrapProfilePosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'profile:getPosts {scrap_type=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Looks for new posts inside a profile page';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(ScrapInstaController $sic)
    {  
        $scrap_type = $this->argument('scrap_type');
        $this->info(date('Y-m-d H:i:s') . 'Inicio de recuperacion de publicaciones');
        $this->scrapProfiles($sic);
        if(scrap_type == 1){
        $this->retrieveDates($sic);
        }
    }
    protected function scrapProfiles(ScrapInstaController $sic){
       $results = $sic->startProfileScrap();
       foreach($results as $result){
        $this->info(date('Y-m-d H:i:s') . $result);
       }
    }
    protected function retrieveDates(ScrapInstaController $sic){
        $results = $sic->retrieveDates();
         $this->info(date('Y-m-d H:i:s') . 'Resultado de la reparacion de fechas ' . 'posts actualizados: ' . $results[1] . ' posts totales: ' . $results[0]);
    }
}
