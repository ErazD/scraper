<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\ScrapInstaController;
use DateTime;

class ScrapInstaMetrics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'InstaScrap:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start scrap of instagram metrics';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(ScrapInstaController $sic)
    {
        //
         $this->info(date("Y-m-d H:i:s") . 'Inicio de scrap Instagram de datos');
         $this->startInstagramScrap($sic);
    }
    protected function startInstagramScrap(ScrapInstaController $sic){
        $result = $sic->scrapPostsMetrics();
        $this->info(date("Y-m-d H:i:s") . 'Scrap de datos terminado' . 'Total Posts: ' . $result[2] . '\n' . 'Post Scrapeados: ' . $result[0] . '\n' . 'Post fallidos: ' . $result[1]);
        foreach($result[3] as  $r){
         $this->info(date("Y-m-d H:i:s") . 'Posts con error en sus metricas' . $r);   
        }

    }
}
