<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\ScrapController;
use DateTime;

class DeleteScrap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DeleteScrap:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deletes data that has already been retrieved by the main database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(ScrapController $controller)
    { 
      $this->deleteScrap($controller);
     
    }
    protected function deleteScrap(ScrapController $controller)
    {
         $controller->deleteData();
         $this->info(date("Y-m-d H:i:s") . " Borrado con éxito");
    }
    
}
