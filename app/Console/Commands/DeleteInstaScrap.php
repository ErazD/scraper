<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\ScrapInstaController;
use DateTime;

class DeleteInstaScrap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
      protected $signature = 'DeleteInstaScrap:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deletes data that has already been retrieved by the main database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(ScrapInstaController $ipc)
    {
        $this->deleteInstaScrap($ipc);
    }
    protected function deleteInstaScrap(ScrapInstaController $ipc)
    {
        $ipc->deleteData();
        $this->info(date("Y-m-d H:i:s") . "Insta borrado con éxito");
    }
}
