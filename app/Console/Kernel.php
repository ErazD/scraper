<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
     protected $commands = [
    \App\Console\Commands\DeleteScrap::class,
    \App\Console\Commands\StartScrap::class,
    \App\Console\Commands\ScrapProfilePosts::class,
    \App\Console\Commands\ScrapInstaMetrics::class,
    \App\Console\Commands\DeleteInstaScrap::class,
    ];


    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    $schedule->command('DeleteScrap:delete')->cron('0 */4 * * *')->appendOutputTo(storage_path('logs/deletelog.log'));
    $schedule->command('DeleteInstaScrap:delete')->monthly()->appendOutputTo(storage_path('logs/deletelog.log'));
    $schedule->command('Scrap:start')->cron('0 */3 * * *')->appendOutputTo(storage_path('logs/scraplog.log'));
   
    $schedule->command('InstaScrap:start')->cron('0 */8 * * *')->appendOutputTo(storage_path('logs/instaScraplog.log'));
    $schedule->command('profile:getPosts')->daily()->appendOutputTo(storage_path('logs/scrapProfiles.log'));

    $schedule->command('profile:getPosts 1')->dailyAt('16:00')->appendOutputTo(storage_path('logs/scrapProfiles.log'));
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
