<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'views-scrapper/',
        'manual-post/',
        'get-posts-bundle/',
        'save-posts-info/',
        'save-insta-profile/',
        'save-data-posts/',
        'get-profile-followers/'
    ];
}
