<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JonnyW\PhantomJs\Client as PhantomClient;
use Symfony\Component\DomCrawler\Crawler;
use App\Profile;
use App\InstagramProfile;
use App\InstagramPost;
use App\InstagramScrap;
use App\InstagramFollower;
use DateTime;
use DateTimeZone;
use GuzzleHttp\Psr7\Request as GuzzRequest;
use GuzzleHttp\Client as GuzzClient; 
use Goutte\Client as GoutteClient;




class ScrapInstaController extends Controller
{

  /**
 * Saves profile general info from scrap database
 * @param {Request} $profiles
 * @param { 
 * @return {string}
 */
  function scrapProfile(Request $profiles){
   $countNew = 0;
   $countOld = 0;

   $data_requests = json_decode($profiles[0], true);

   foreach ($data_requests as $data_request) {
     $scrap_profile = Profile::where('id', $data_request['profile_id'])->first();

     if(!is_null($scrap_profile)) {
       $countOld++;
     }else{

     //Se crean los valores en la base de datos
      Profile::create([
       'id' => $data_request['profile_id'],
       'profile_id' => $data_request['profile_id'],
       'profile_url' => $data_request['profile_url']
       ]);
      $countNew++;
    }
  }
  $result = [$countNew, $countOld];
  return $result;
}   

/**
 * Save general posts info to scrap their metrics
 * @param {Request} $posts  
 * @return {string}
 */

function savePostInfo(Request $posts){

  $posts_data = json_decode($posts[0], true);
  $index = 0;
  foreach($posts_data as $post_data){
   if(!empty($post_data)){

     $scrap_post = InstagramScrap::firstOrNew(array('post_id' => $post_data['post_id']));
     $scrap_post->type = $post_data['type'];
     $state = $post_data['scrap_state'];
     if($state>0){
       $scrap_post->sent = 10;
     }else{
       $scrap_post->sent = 0;
     }
     $scrap_post->save();
     $index++;
   }
 }
 return 'Posts agregados para scrapear: ' . $index;
}

/**
 * Begins scraping of Instagram posts metrics saved on database 
 * @return {array}  Scrap results - failed scraps, successful scraps, total scraps
 */

function savePostsDates(Request $posts){
  $posts_data = json_decode($posts[0], true);
  $index = 0;
  foreach($posts_data as $post_data){
   if(!empty($post_data)){
     $scrap_post = InstagramPost::firstOrNew(array('post_id' => $post_data['post_id']));
     $scrap_post->profile_id = $post_data['profile_id'];
     $scrap_post->published = $post_data['published'];
     $scrap_post->post_id = $post_data['post_id'];
     $scrap_post->permalink_url = $post_data['permalink_url'];
     $scrap_post->content = $post_data['content'];
     $scrap_post->type = $post_data['type'];
     $scrap_post->save();
     $index++;
   }
 }
 return 'Posts agregados para scrapear fechas: ' . $index;
}

// function retrieveDates(){
//  $fechasES = ['de enero', 'de febrero', 'de marzo', 'de abril', 'de mayo', 'de junio', 'de julio', 'de agosto', 'de septiembre', 'de octubre', 'de noviembre', 'de diciembre'];
//  $fechasESB = ['de enero de', 'de febrero de', 'de marzo de', 'de abril de', 'de mayo de', 'de junio de', 'de julio de', 'de agosto de', 'de septiembre de', 'de octubre de', 'de noviembre de', 'de diciembre de'];
//  $fechasEN = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
//  $dateBreakers = ['ago', 'AGO', 'today', 'Today', 'TODAY', 'hace', 'HACE', 'Hace', 'Hoy', 'hoy', 'HOY']; 

//  $client = PhantomClient::getInstance();    
//    // $client->getEngine()->setPath('/var/http/owak/live/scraper/bin/phantomjs');
//  $client->getEngine()->setPath(base_path().'/bin/phantomjs');
//  $client->isLazy();
//  $posts = InstagramPost::all();
//  $delay = 10;
//  $countFail = 0;
//  $countTotal = 0;
//  $countSuccess = 0;

//  foreach($posts as $post){
//   $request = $client->getMessageFactory()->createRequest('https://www.instagram.com/p/'. $post->post_id, 'GET');
//   $request->setViewportSize(1280, 920);
//   $response = $client->getMessageFactory()->createResponse();
//   $request->setDelay($delay); 
//   $client->send($request, $response);

//   if($response->getStatus() == 301){
//     $html = $response->getContent();
//     $crawler = new Crawler();
//     $crawler->addHTMLContent($html);

//       //Extract Comments
//       /////////////////////////////////////////
//     $published = 'a.c-Yi7 time';
//     $date = ''; 
//     //Extract Publishing date  
//     $extractDate = $crawler->filter($published)->extract('_text');
//     $extractInnerDate = $crawler->filter($published)->attr('datetime');
//     $match = str_replace($dateBreakers, '', $extractDate);

//     $dealBreaker = ($match != $extractDate)? 'true' : 'false';

//     if(str_word_count($extractDate[0], 0) > 2  && $dealBreaker === false){
//       $date = new DateTime(str_replace($fechasESB, $fechasEN, $extractDate[0]));
//     }else  if(str_word_count($extractDate[0], 0) < 2 &&  $dealBreaker === false){
//       $date = new DateTime(str_replace($fechasES, $fechasEN, $extractDate[0]));
//     }else{
//       $dtNow = new DateTime();
//       $dtTimeZone = date_timezone_get($dtNow);
//       $dateTemp = new DateTime($extractInnerDate);
//       $date  =  (date_timezone_set($dateTemp, $dtTimeZone));
//     }

//      // ////////////////////////////////////////
//     $countTotal++;

//     $save_date = InstagramPost::firstOrNew(array('post_id' => $post->post_id));
//     if($save_date->published != $date){
//         $save_date->published = $date;
//         $save_date->save();
//         $countSuccess++;
//     }else{

//     }
//     sleep(10);
//   }
// }
//  return array($countTotal, $countSuccess);
// }

function scrapPostsMetrics(){
 $client = PhantomClient::getInstance();    
   // $client->getEngine()->setPath('/var/http/owak/live/scraper/bin/phantomjs');
 $client->getEngine()->setPath(base_path().'/bin/phantomjs');
 $client->isLazy();
 $posts = InstagramScrap::where('likes', '=', 0)->orWhere('views', '=', 0)->whereIn('sent', [-6, -5, -4, -3, -2, -1, 0, 2, 3, 4, 5, 6, 7, 8, 9, 10])->get();

 $delay = 10;
 $countScraped = 0;
 $countFail = 0;
 $countFailPhoto = [];
 $countTotal = 0;

 foreach($posts as $post){
  $request = $client->getMessageFactory()->createRequest('https://www.instagram.com/p/'. $post->post_id, 'GET');
  $request->setViewportSize(1280, 920);
  $response = $client->getMessageFactory()->createResponse();
  $request->setDelay($delay); 
  $client->send($request, $response);

  if($response->getStatus() == 301){
    $html = $response->getContent();
    $crawler = new Crawler();
    $crawler->addHTMLContent($html);
    $type = 'div.HbPOm';
    // $comments = 'li.gElp9';

    $metric = 'div.HbPOm span';
    $metricB = 'meta';
    $temp = $crawler->filter($metricB)->extract('content');
      //Extract Comments
    $commentM = '';
    $extractComment = explode(',', $temp[6]);
    if(strpos($extractComment[1], 'Comments')){
      $commentM = explode('Comments', $extractComment[1]);
    }else{
      $commentM = explode('comentarios', $extractComment[1]);
    }
    // $commentM = sizeof($extractComment) - 1 ;
    //Extract Metrics
    $extractType = $post->type;
    $extractMetric = '';
    $extractLikes= '';
    
    if($extractType == 'video'){
     $extractMetric = $crawler->filter($metric)->extract('_text'); 
     $extractLikes = $this->extractLikes($temp[6]);
   }else{ 
    try{
     if($temp[6]){ 
       if(strpos($temp[6], 'Me gusta')){
         $extractMetric = explode('Me gusta', $temp[6]);
       }else{
         $extractMetric = explode('Likes', $temp[6]);
       }
     }else{
       array_push($countFailPhoto, $post->post_id);
       $post->sent = $post->sent - 1;
     }
   }catch (Exception $e) {
     $this->error('Error en '.$post->post_id);
   }
 }

  

 if(!empty($extractMetric) && !empty($extractComment)){
  $post->comments = intval(preg_replace('/[^\d.]/', '', $commentM[0]));
  if($post->type == 'video'){
   $post->views = intval(preg_replace('/[^\d.]/', '', $extractMetric[0]));
   $post->likes = intval(preg_replace('/[^\d.]/', '', $extractLikes[0]));
 }else{
  $post->likes = intval(preg_replace('/[^\d.]/', '', $extractMetric[0]));
}
$countScraped++;
}else{
 $post->sent = $post->sent - 1;
}
$countTotal++;
$post->save();
}
sleep(10);
}
return array($countScraped, $countFail, $countTotal, $countFailPhoto);
}    

function extractLikes($scrapLikes){
  $extractLikes = 0;
 try{
  if($scrapLikes){ 
   if(strpos($scrapLikes, 'Me gusta')){
     $extractLikes = explode('Me gusta', $scrapLikes);
   }else{
     $extractLikes = explode('Likes', $scrapLikes);
   }
 }
}catch (Exception $e) {
     $this->error('Error');
}
 return $extractLikes;   
}
/**
 * Returns new post metrics to the main database
 * @return {array}  new metrics from scraped posts
 */
function sendMetrics(){
  $container_metrics = [];

  $scrap_metrics = InstagramScrap::where('likes', '>' , 0)->orWhere('views', '>', 0)->get();
  foreach($scrap_metrics as $scrap_metric){
  
    $response_pack = [
    'post_id' => $scrap_metric->post_id,
    'type' => $scrap_metric->type,
    'likes' => $scrap_metric->likes,
    'views' => $scrap_metric->views,
    'comments' => $scrap_metric->comments
    ];

    array_push($container_metrics, $response_pack);

    $scrap_metric->sent = 1;
    $scrap_metric->save();
  }

  return $container_metrics;
}

function sendInstaFollowers(){
$container_followers = [];
$followers = InstagramFollower::where('sent', '=', 0)->get();
foreach($followers as $follower){
   $response_pack =[
   'profile_id' => $follower->profile_id,
   'metric' => $follower->metric
   ];
  array_push($container_followers, $response_pack);
  $follower->sent = 1;
  $follower->save();
}
return $container_followers;
}

/**
 * Returns new scraped posts to the main database
 * @return {array}  new posts from scraped profiles
 */
function sendPosts(){
 $container_posts = [];

 $scrap_posts = InstagramPost::where('sent' , '=', 0)->get();
 
 foreach($scrap_posts as $scrap_post){
   $response_pack = [
   'profile_id' => $scrap_post->profile_id,
   'post_id' => $scrap_post->post_id,
   'attachment' => $scrap_post->attachment,
   'content' => $scrap_post->content,
   'type' => $scrap_post->type,
   'likes' => $scrap_post->likes,
   'views' => $scrap_post->views,
   'comments' => $scrap_post->comments,
   'published' => $scrap_post->published,
   'url' => $scrap_post->permalink_url
   ];

   array_push($container_posts, $response_pack);

   $scrap_post->sent = 1;
   $scrap_post->save();

 }

 return $container_posts;
}


/**
 * Scraps the urls of recent posts from Instagram Profile
 * @return {array} new instagram posts general info
 */

function startProfileScrap(){
 $profiles = Profile::all();
 $profileMessages = [];
 
 foreach($profiles as $profile){

   $posts = [];
   $p_url = $profile->profile_url;
   $p_id = $profile->profile_id;
   $client = PhantomClient::getInstance();
   $client->getEngine()->setPath(base_path().'/bin/phantomjs');
   $client->isLazy();
   $delay = 10;
   $request = $client->getMessageFactory()->createRequest('https://www.instagram.com/'. $p_url, 'GET'); 
   $request->setViewportSize(1280, 920);
   $response = $client->getMessageFactory()->createResponse();
   $request->setDelay($delay); 
   $client->send($request, $response);

   if($response->getStatus() == 301){
    $html = $response->getContent();
    $link_location ='div.kIKUG a';
    $followers = 0;
    $crawler = new Crawler();
    $crawler->addHTMLContent($html);
    $hrefs = $crawler->filter($link_location)->extract('href');
    $follower_location = 'span.g47SY';  
    $followMeta = $crawler->filter($follower_location)->extract('title');
    if(!empty($followMeta) && sizeof($followMeta) > 0){
    $i_follower = new InstagramFollower();
    $i_follower->add(intval(preg_replace('/[^\d.]/', '', $followMeta[1])), $p_id);
    }
    $indicator = 1;
    try{
      $posts = $this->getPostsData($hrefs, $p_id, $indicator);

    }catch(Exception $e){
     $this->error($e);     
   }  
   array_push($profileMessages, $posts);

 }
}
return $profileMessages;
}

function scrapProfilePosts(Request $profile){
 $post_request = json_decode($profile[0], true);
 $p_url = $post_request['profile_url'];
 $p_id =  $post_request['profile_id'];
 $posts = [];
 $client = PhantomClient::getInstance();
 $client->getEngine()->setPath(base_path().'/bin/phantomjs');
 $client->isLazy();
 $delay = 10;
 $request = $client->getMessageFactory()->createRequest('https://www.instagram.com/'. $p_url, 'GET'); 
 $request->setViewportSize(1280, 920);
 $response = $client->getMessageFactory()->createResponse();
 $request->setDelay($delay); 
 $client->send($request, $response);

 if($response->getStatus() == 301){
  $html = $response->getContent();
  $link_location ='div.kIKUG a';
  $crawler = new Crawler();
  $crawler->addHTMLContent($html);
  $hrefs = $crawler->filter($link_location)->extract('href');

  $posts = $this->getPostsData($hrefs, $p_id);
  
  return $posts;
}
}

/**
 * Retrieves general post data from recent posts found on instagram profile
 * @param {string} $hrefs -> retrieved links from recent instagram posts  
 * @param {string} $p_id -> Current profile being scraped
 * @return {array}  New posts general info
 */
function getPostsData($hrefs, $p_id, $indicator = 0){

 $fechasES = ['de enero', 'de febrero', 'de marzo', 'de abril', 'de mayo', 'de junio', 'de julio', 'de agosto', 'de septiembre', 'de octubre', 'de noviembre', 'de diciembre'];
 $fechasESB = ['de enero de', 'de febrero de', 'de marzo de', 'de abril de', 'de mayo de', 'de junio de', 'de julio de', 'de agosto de', 'de septiembre de', 'de octubre de', 'de noviembre de', 'de diciembre de'];
 $fechasEN = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
 $dateBreakers = ['ago', 'AGO', 'today', 'Today', 'TODAY', 'hace', 'HACE', 'Hace', 'Hoy', 'hoy', 'HOY']; 
 $width = 480;
 $height = 600;
 $top = 115;
 $left = 225;
 $delay = 30;
 $posts = [];
 $addPost = 0;

 foreach($hrefs as $href){
  $post_id = explode('/', $href);
  $imgUrl = '/uploads/Instagram/'. $post_id[2] . '-' . $p_id .'.jpg';
  $client = PhantomClient::getInstance();
  $client->getEngine()->setPath(base_path().'/bin/phantomjs');
  $client->isLazy(); 
  $request = $client->getMessageFactory()->createCaptureRequest('https://www.instagram.com/p/'. $post_id[2], 'GET'); 
  $request->setOutputFile(public_path($imgUrl));
  $request->setCaptureDimensions($width, $height, $top, $left);
  $response = $client->getMessageFactory()->createResponse();
  $request->setDelay($delay);
  $request->setViewportSize(1280, 320);
  $client->send($request, $response);

  if($response->getStatus() == 301){
    $html = $response->getContent();
    $crawler = new Crawler();
    $crawler->addHTMLContent($html);

      //Elementos HTML a extraer
    $content = 'li.gElp9';
    //$comments = 'li.gElp9';
    $type = 'div.HbPOm';
    $typeCategory = '';
    $metric = 'div.HbPOm span';
    // $metricB = 'div.HbPOm.y9v3U a.zV_Nj span';
    $metricB = 'meta';
    $published = 'a.c-Yi7 time';
    $comments = 0;
    $date = '';
    $img_route = asset($imgUrl);
    $temp = $crawler->filter($metricB)->extract('content');
    //Extract content and general info 
    $extractContent = $crawler->filter($content)->extract('_text');
    $extractType = $crawler->filter($type)->extract('_text');
    $extractLikes = '';
    //Extract Metric
    $extractMetric = '';
    if(sizeof($extractType) > 0){
     $typeCategory = 'video';
     $extractMetric = $crawler->filter($metric)->extract('_text'); 
     $extractLikes =  $this->extractLikes($temp[6]);
   }else{
     $typeCategory = 'photo';
     $extractMetric =  $this->extractLikes($temp[6]);
   }

    //Extract Publishing date  
   $extractDate = $crawler->filter($published)->extract('_text');
   $extractInnerDate = $crawler->filter($published)->attr('datetime');
   $match = str_replace($dateBreakers, '', $extractDate);

   $dealBreaker = ($match != $extractDate)? 'true' : 'false';

   if(str_word_count($extractDate[0], 0) > 2  && $dealBreaker === false){
    $date = new DateTime(str_replace($fechasESB, $fechasEN, $extractDate[0]));
  }else  if(str_word_count($extractDate[0], 0) < 2 &&  $dealBreaker === false){
    $date = new DateTime(str_replace($fechasES, $fechasEN, $extractDate[0]));
  }else{
    $dtNow = new DateTime();
    $dtTimeZone = date_timezone_get($dtNow);
    $dateTemp = new DateTime($extractInnerDate);
    $date  =  (date_timezone_set($dateTemp, $dtTimeZone));
  }

  $extractComment = explode(',', $temp[6]);
  if(strpos($extractComment[1], 'Comments')){
    $comments = explode('Comments', $extractComment[1]);
  }else{
    $comments = explode('Comentarios', $extractComment[1]);
  }

  if($indicator == 0){
    //Preparación de paquete
  $postData = [
   'post_id' => $post_id[4],
   'content' => (count($extractContent) > 0)? preg_replace('/[^(\x20-\x7F)]*/', '', $extractContent[0]) : null,
   'type' => $typeCategory,
   'url' => $data['post_url'],
   'likes' => ($typeCategory == 'photo')? intval(preg_replace('/[^\d.]/', '', $extractMetric[0])) : intval(preg_replace('/[^\d.]/', '', $extractLikes[0])),
   'views' => ($typeCategory == 'video')? intval(preg_replace('/[^\d.]/', '', $extractMetric[1])) : 0,
   'comments' =>  intval(preg_replace('/[^\d.]/', '', $comments[0])),   
   'published' => $date,
   'attachment' => $img_route
   ];
  
  array_push($posts, $postData);

  }else{

    $checkIfExists = InstagramPost::where('post_id', $post_id[2])->first();
    
    if($checkIfExists){

    //si existe no haga nada

    }else{

      $postData = [ 
      'post_id' => $post_id[2],
      'content' => (count($extractContent) > 0)? preg_replace('/[^(\x20-\x7F)]*/', '', $extractContent[0]) : null,
      'type' => $typeCategory,
      'url' =>  'https://www.instagram.com'.$href,
      'likes' => ($typeCategory == 'photo')? intval(preg_replace('/[^\d.]/', '', $extractMetric[0])) : intval(preg_replace('/[^\d.]/', '', $extractLikes[0])),
      'views' => ($typeCategory == 'video')? intval(preg_replace('/[^\d.]/', '', $extractMetric[1])) : 0,
      'comments' =>  intval(preg_replace('/[^\d.]/', '', $comments[0])),   
      'published' => $date,
      'attachment' => $img_route
      ];
      $ip = new InstagramPost();
      $ip->add($postData, $p_id); 
      $addPost++;    
    } 
  }

}
sleep(10);
}

if($indicator == 0){
  return $posts;
}else{
  return 'Publicaciones nuevas encontradas: ' . $addPost . ' Para el perfil ' . $p_id; 
}

}  

/**
 * Manually scraps general data of a single post
 * @param {Request} $post  
 * @return {array}  New posts general info
 */
function scrapManualPost(Request $post){
  if($post){
   $data = json_decode($post[0], true);
      //$post_id[4]
   $post_id = explode('/', $data['post_url']);
   $fechasES = ['de enero', 'de febrero', 'de marzo', 'de abril', 'de mayo', 'de junio', 'de julio', 'de agosto', 'de septiembre', 'de octubre', 'de noviembre', 'de diciembre'];
   $fechasESB = ['de enero de', 'de febrero de', 'de marzo de', 'de abril de', 'de mayo de', 'de junio de', 'de julio de', 'de agosto de', 'de septiembre de', 'de octubre de', 'de noviembre de', 'de diciembre de'];
   $fechasEN = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
   $dateBreakers = ['ago', 'AGO', 'today', 'Today', 'TODAY', 'hace', 'HACE', 'Hace', 'Hoy', 'hoy', 'HOY']; 

   $client = PhantomClient::getInstance();

   $width = 480;
   $height = 600;
   $top = 115;
   $left = 230;
   $imgUrl = '/uploads/Instagram/'. $post_id[4] . '-' . $data['profile_id'] .'.jpg';
   $client->getEngine()->setPath(base_path().'/bin/phantomjs');
   $client->isLazy();
   $delay = 10;
   $request = $client->getMessageFactory()->createCaptureRequest('https://www.instagram.com/p/'. $post_id[4], 'GET');  
   $request->setOutputFile(public_path($imgUrl));
   $request->setCaptureDimensions($width, $height, $top, $left);
   $response = $client->getMessageFactory()->createResponse();
   $request->setDelay($delay);
   $request->setViewportSize(1280, 920); 
   $client->send($request, $response);

   if($response->getStatus() == 301){
    $html = $response->getContent();
    $crawler = new Crawler();
    $crawler->addHTMLContent($html);

      //Elementos HTML a extraer
    $content = 'li.gElp9';
      //$comments = 'li.gElp9';
    $type = 'div.HbPOm';
    $typeCategory = '';
    $metric = 'div.HbPOm span';
    $metricB = 'meta';
    $extractMetric = '';
    $published = 'a.c-Yi7 time';
    // $comments = 0;
    $date = '';
    $img_route = asset($imgUrl);
    $temp = $crawler->filter($metricB)->extract('content');
    //Extract general info
    $extractContent = $crawler->filter($content)->extract('_text');
    $extractType = $crawler->filter($type)->extract('_text');
    
    $extractLikes = '';
    //Extract metric
    if(sizeof($extractType) > 0){
     $typeCategory = 'video';
     $extractMetric = $crawler->filter($metric)->extract('_text'); 
     $extractLikes =  $this->extractLikes($temp[6]);
   }else{
     $typeCategory = 'photo';
     $extractMetric  = $this->extractLikes($temp[6]);
   }

    //Extract Publishing date
   $extractDate = $crawler->filter($published)->extract('_text');
   $extractInnerDate = $crawler->filter($published)->attr('datetime');
   $match = str_replace($dateBreakers, '', $extractDate);

   $dealBreaker = ($match != $extractDate)? 'true' : 'false';

   if(str_word_count($extractDate[0], 0) > 2  && $dealBreaker === false){
    $date = new DateTime(str_replace($fechasESB, $fechasEN, $extractDate[0]));
  }else  if(str_word_count($extractDate[0], 0) < 2 &&  $dealBreaker === false){
    $date = new DateTime(str_replace($fechasES, $fechasEN, $extractDate[0]));
  }else{
    $dtNow = new DateTime();
    $dtTimeZone = date_timezone_get($dtNow);
    $dateTemp = new DateTime($extractInnerDate);
    $date  =  (date_timezone_set($dateTemp, $dtTimeZone));
  }

    //Extract Comments
   $extractComment = explode(',', $temp[6]);
    if(strpos($extractComment[1], 'Comments')){
      $comments = explode('Comments', $extractComment[1]);
    }else{
      $comments = explode('comentarios', $extractComment[1]);
   }
      
    //Post info package
  $postData = [
  'post_id' => $post_id[4],
  'content' => (count($extractContent) > 0)? preg_replace('/[^(\x20-\x7F)]*/', '', $extractContent[0]) : null,
  'type' => $typeCategory,
  'url' => $data['post_url'],
  'likes' => ($typeCategory == 'photo')? intval(preg_replace('/[^\d.]/', '', $extractMetric[0])) : intval(preg_replace('/[^\d.]/', '', $extractLikes[0])),
  'views' => ($typeCategory == 'video')? intval(preg_replace('/[^\d.]/', '', $extractMetric[1])) : 0,
  'comments' =>  intval(preg_replace('/[^\d.]/', '', $comments[0])),   
  'published' => $date,
  'attachment' => $img_route
  ];

  return $postData;
}
}else{
  return 'Llego vacio';
}
}

/**
 * Deletes data that has already been sent to the main database 
 * @return {void} 
 */
function deleteData(){
 $sent_post_scrap = InstagramPost::where('sent', '=', 1)->delete();
 $sent_insta_scrap = InstagramScrap::where('sent', '=', 1)->delete();
 $sent_insta_follower = InstagramFollower::where('sent', '=', 1)->delete();
}


function sendFrozenInsta(){
 $container_frozen=[];
 $frozen_scraps = InstagramScrap::where('sent', '<=', '-6')->where('views', 0)->where('likes', 0)->where('comments', 0)->get();
 foreach($frozen_scraps as $frozen_scrap){
   $response_pack =[
   'post_id' => $frozen_scrap->post_id,
   'scrap_state' => $frozen_scrap->sent
   ];

   array_push($container_frozen, $response_pack);
 }
 return $container_frozen; 
}

/*-------------------------
Debug method to check for errors on the scraping of posts

--------------------------*/
 function debugScrapInitialPosts(){ 
   // $p_id = 27;
   $p_url =  'babydove_co';
   $meta = 'meta';
  

   $client = PhantomClient::getInstance();
   $client->getEngine()->setPath(base_path().'/bin/phantomjs');
   $client->isLazy();
   $delay = 10;
   $request = $client->getMessageFactory()->createRequest('https://www.instagram.com/'. $p_url, 'GET'); 
   $request->setViewportSize(1280, 920);
   $response = $client->getMessageFactory()->createResponse();
   $request->setDelay($delay); 
   $client->send($request, $response);

   if($response->getStatus() == 301){
    $html = $response->getContent();
    $link_location ='div.kIKUG a';
    $follower_location = 'span.g47SY';
    $crawler = new Crawler();
    $crawler->addHTMLContent($html);
    // dd('Lo que obteien el crawler', $crawler); 
    // $meta = $crawler->filter($meta)->extract('content');
    $followMeta = $crawler->filter($follower_location)->extract('title');
    // dd($followMeta);
    $followers = 0;
    // $followers =  explode('seguidores', $meta[12]);
    // if(strpos($meta[12], 'seguidores')){
    //   $followers = explode('seguidores', $meta[12]);
    // }else{
    //   $followers = explode('followers', $meta[12]);
    // }
    // dd($followMeta);
    if(!empty($followMeta) && sizeof($followMeta) > 0){
    dd(intval(preg_replace('/[^\d.]/', '', $followMeta[1]))); 
    }else{
    dd('parece que no existe');
    }
    // if(strpos($followers[0], 'mil') || strpos($followers[0], 'K')){
    //    $mil = explode(' ', $followers[0]);
    //    $milR = (float) $mil[0] * 1000;
    //    dd('seguidores:', (int) $milR);
    // }else if(strpos($followers[0], 'millon') || strpos($followers[0], 'M')){
    //   $mill = intval(preg_replace('/[^\d.]/', '', $followers[0])) * 1000000;
    //   dd('seguidores', $mill);
    // }else{
    //   $result = intval(preg_replace('/[^\d.]/', '', $followers[0]));
    //   dd('seguidores:', $result);
    // }


    // foreach($measures as $measure){
    //   if(strpos($followers[0], $measure)){
    //       dd('seguidores: ', intval(preg_replace('/[^\d.]/', '', $followers[0])));
    //   }
    // }
    // if(strpos($followers[0], 'K')){

    // }
    // dd('seguidores: ', intval(preg_replace('/[^\d.]/', '', $followers[0])));
    // dd('numero de seguidores:', intval($followers[0]));
    // dd('Meta obtenido', $meta);
    // $this->getPostsData($hrefs, $p_id);
  }
}


/*----------------------------
Function created to debug the scrapManualPost method
----------------------------*/

// function debugScrap(){

//      //$data = json_decode($post[0], true);
//  $data = [
//  'profile_id' => 1,
//  'post_url' => 'https://www.instagram.com/p/BgE7tgNHLL9'
//  ];

//  $chrome_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36';
// $firefox_agent = 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0';
// $ie_agent = 'Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko';
// $edge_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063';
// $agents = array($chrome_agent, $firefox_agent, $edge_agent, $ie_agent);

//      //$post_id[4]
//  $post_id = explode('/', $data['post_url']);
//  $fechasES = ['de enero', 'de febrero', 'de marzo', 'de abril', 'de mayo', 'de junio', 'de julio', 'de agosto', 'de septiembre', 'de octubre', 'de noviembre', 'de diciembre'];
//  $fechasESB = ['de enero de', 'de febrero de', 'de marzo de', 'de abril de', 'de mayo de', 'de junio de', 'de julio de', 'de agosto de', 'de septiembre de', 'de octubre de', 'de noviembre de', 'de diciembre de'];
//  $fechasEN = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

//  $client = PhantomClient::getInstance();

//  $width = 480;
//  $height = 600;
//  $top = 115;
//  $left = 225;
//  $imgUrl = '/uploads/Instagram/'. $post_id[4] . '-' . 'TEST' .'.jpg';
//  $client->getEngine()->setPath(base_path().'/bin/phantomjs');
//  $delay = 20;
//  $request = $client->getMessageFactory()->createCaptureRequest('https://www.instagram.com/p/'.$post_id[4], 'GET');  
//  $request->setOutputFile(public_path($imgUrl));
//  $request->setCaptureDimensions($width, $height, $top, $left);
//  $user_agent = array_rand(array_flip($agents));
//  $request->addSetting('userAgent', $user_agent);
//  $response = $client->getMessageFactory()->createResponse();
//  $request->setDelay($delay);
//  $request->setViewportSize(1280, 920); 
//  $client->send($request, $response);

//  if($response->getStatus() == 301){

//   $html = $response->getContent();
//   dd($html);
//   $crawler = new Crawler();
//   $crawler->addHTMLContent($html);

//   $content = 'meta';

//   $contenido = $crawler->filter($content)->extract('content');



//   // $crawler = new Crawler();
//   // $crawler->addHTMLContent($html);

//   //     //Elementos HTML a extraer
//   // $content = 'li.gElp9';
//   //     //$comments = 'li.gElp9';
//   // $type = 'div.HbPOm';
//   // $typeCategory = '';
//   // $metric = 'div.HbPOm span';
//   // $published = 'a.c-Yi7 time';
//   // $comments = 0;
//   // $date = '';
//   // $img_route = asset($imgUrl);

//   //     //Contenido Extraido 
//   // $extractContent = $crawler->filter($content)->extract('_text');
//   // $extractMetric = $crawler->filter($metric)->extract('_text');
//   // $extractType = $crawler->filter($type)->extract('_text');
//   // $extractDate = $crawler->filter($published)->extract('_text');
//   // if(str_word_count($extractDate[0], 0) > 2){
//   //   $date = new DateTime(str_replace($fechasESB, $fechasEN, $extractDate[0]));
//   // }else{
//   //   $date = new DateTime(str_replace($fechasES, $fechasEN, $extractDate[0]));
//   // }
//   // $comments = sizeof($extractContent) - 1;  
//   // if(strpos($extractType[0], 'likes') || strpos($extractType[0], 'Me gusta')){
//   //   $typeCategory = 'photo';
//   // }else{
//   //   $typeCategory = 'video'; 
//   // }

//   //     //Preparación de paquete
//   // $postData = [
//   // 'post_id' => $post_id[4],
//   // 'content' => $extractContent[0],
//   // 'type' => $typeCategory,
//   // 'url' => $data['post_url'],
//   // 'metric' => $extractMetric[1],
//   // 'comments' =>  $comments,   
//   // 'published' => $date,
//   // 'attachment' => $img_route
//   // ];

//   // dd($postData);
// }

// }



}