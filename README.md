# README #

Scraper project with extra features for Owak Graph

### What is this repository for? ###

Scrap relevant data of Facebook and Instagram for Owakgraph

### How do I get set up? ###

* Download the project and reinstall php phantomjs (guide to do so: http://phantomjs.org/ download the exec file and locate it on the bin folder), on the .env file fill the data regarding your database then run php migrate to create the database tables and run the project on your local server. The current location is (scraper.test:8888) if there's issues with that location run php artisan serve on the console.


### Contribution guidelines ###

* The project runs differently from the one live (mainly the urls and routes) so everytime you make a new feature it must be tested and modified on live as well.
* The live project is in continuous communication with owakgraph so most of it's features handle data transactions between the two projects. The bug report of this features come out incomplete because the communication is severed in the middle of the process, to test the features out use the debug functions to test in Scraper (/debug-metrics, /debug-posts-bundle, /debug-scrap)

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact